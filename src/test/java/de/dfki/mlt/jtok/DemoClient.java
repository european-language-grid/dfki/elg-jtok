package de.dfki.mlt.jtok;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.elg.model.AnnotationObject;
import eu.elg.model.requests.TextRequest;
import eu.elg.model.responses.AnnotationsResponse;


/**
 * Simple demo client that sends a {@link eu.elg.model.requests.TextRequest}
 * to the JTok ELG wrapper and gets back an {@link eu.elg.model.responses.AnnotationsResponse}
 * with tokens, text units and paragraphs.
 *
 * @author Jörg Steffen, DFKI
 */
public class DemoClient {

  private static final Logger logger = LoggerFactory.getLogger(DemoClient.class);


  /**
   * The main method.
   *
   * @param args
   *          the arguments; not used here
   */
  public static void main(String[] args) {

    try {
      ObjectMapper objectMapper = new ObjectMapper();

      // init text request
      TextRequest textRequest = new TextRequest();
      textRequest.setContent(
          "This is the text to tokenize. And here some more text.\n\nNew Paragraph");
      logger.info(String.format("text request as JSON:%n%s%n",
          objectMapper.writeValueAsString(textRequest)));

      // send text request to ELG JTok wrapper
      HttpClient httpClient = HttpClients.createDefault();
      StringEntity requestEntity =
          new StringEntity(
              objectMapper.writeValueAsString(textRequest), ContentType.APPLICATION_JSON);
      URI uri = new URI(
          "http",
          null,
          "localhost", 8080, "/en",
          null,
          null);
      HttpPost postMethod = new HttpPost(uri);
      postMethod.setEntity(requestEntity);

      HttpResponse response = httpClient.execute(postMethod);
      int status = response.getStatusLine().getStatusCode();
      if (status >= 200 && status < 300) {
        HttpEntity entity = response.getEntity();
        if (entity != null) {
          String jsonResultString = EntityUtils.toString(entity);
          // the actual annotations response is nested in 'response'
          JsonNode resultJsonNode = objectMapper.readTree(jsonResultString);
          JsonNode annoString = resultJsonNode.get("response");
          logger.info(String.format("annotations response as JSON:%n%s%n", annoString));
          // unmarshall to annotations response
          AnnotationsResponse annotationResponse =
              objectMapper.treeToValue(annoString, AnnotationsResponse.class);
          // print paragraphs
          List<AnnotationObject> paras = annotationResponse.getAnnotations().get("paragraphs");
          logger.info("paragraphs:");
          for (AnnotationObject onePara : paras) {
            logger.info(String.format("%s-%s", onePara.getStart(), onePara.getEnd()));
          }
          // print text units
          List<AnnotationObject> textUnits = annotationResponse.getAnnotations().get("text_units");
          logger.info("text units:");
          for (AnnotationObject oneTextUnit : textUnits) {
            logger.info(String.format("%s-%s", oneTextUnit.getStart(), oneTextUnit.getEnd()));
          }
          // print tokens
          List<AnnotationObject> tokens = annotationResponse.getAnnotations().get("tokens");
          logger.info("tokens:");
          for (AnnotationObject oneToken : tokens) {
            logger.info(String.format("%s-%s %s %s",
                oneToken.getStart(), oneToken.getEnd(),
                oneToken.getFeatures().get("image"),
                oneToken.getFeatures().get("type")));
          }
        }
      }

    } catch (URISyntaxException | IOException e) {
      logger.error(e.getLocalizedMessage(), e);
    }
  }
}
