package de.dfki.mlt.jtok;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.dfki.lt.tools.tokenizer.JTok;
import de.dfki.lt.tools.tokenizer.annotate.AnnotatedString;
import de.dfki.lt.tools.tokenizer.output.Outputter;
import de.dfki.lt.tools.tokenizer.output.Paragraph;
import de.dfki.lt.tools.tokenizer.output.TextUnit;
import de.dfki.lt.tools.tokenizer.output.Token;
import eu.elg.handler.ElgHandler;
import eu.elg.handler.ElgHandlerRegistrar;
import eu.elg.handler.ElgHandlerRegistration;
import eu.elg.model.AnnotationObject;
import eu.elg.model.requests.TextRequest;
import eu.elg.model.responses.AnnotationsResponse;

/**
 * Handler for JTok tokenization requests.
 *
 * @author Jörg Steffen, DFKI
 */
@Component
@ElgHandler
public class JTokHandler implements ElgHandlerRegistration {

  private JTok tokenizer;


  /**
   * Create a new handler instance.
   *
   * @throws IOException
   *           if JTok initialization fails
   */
  @Autowired
  public JTokHandler()
      throws IOException {

    this.tokenizer = new JTok();
  }


  /**
   * Process tokenization request.
   *
   * @param request
   *          the request holding the text to tokenize.
   * @param lang
   *          the language
   * @return annotation response with tokens, text units and paragraphs
   * @throws Exception
   *           if an error occurs
   */
  public AnnotationsResponse process(TextRequest request, String lang)
      throws Exception {

    String inputText = request.getContent();

    List<AnnotationObject> paras = new ArrayList<>();
    List<AnnotationObject> textUnits = new ArrayList<>();
    List<AnnotationObject> tokens = new ArrayList<>();

    AnnotatedString annotatedString = this.tokenizer.tokenize(inputText, lang);
    for (Paragraph onePara : Outputter.createParagraphs(annotatedString)) {
      paras.add(new AnnotationObject().withOffsets(onePara.getStartIndex(), onePara.getEndIndex()));
      for (TextUnit oneTextUnit : onePara.getTextUnits()) {
        textUnits.add(
            new AnnotationObject()
                .withOffsets(oneTextUnit.getStartIndex(), oneTextUnit.getEndIndex()));
        for (Token oneToken : oneTextUnit.getTokens()) {
          tokens.add(
              new AnnotationObject()
                  .withOffsets(oneToken.getStartIndex(), oneToken.getEndIndex())
                  .withFeature("image", oneToken.getImage())
                  .withFeature("type", oneToken.getType()));
        }
      }
    }

    AnnotationsResponse response = new AnnotationsResponse();
    response.withAnnotations("tokens", tokens);
    response.withAnnotations("text_units", textUnits);
    response.withAnnotations("paragraphs", paras);

    return response;
  }


  @Override
  public void registerHandlers(ElgHandlerRegistrar registrar) {

    registrar.handler("/de", TextRequest.class, r -> process(r, "de"));
    registrar.handler("/en", TextRequest.class, r -> process(r, "en"));
    registrar.handler("/it", TextRequest.class, r -> process(r, "it"));
  }
}
