# JTok ELG Wrapper

This is a simple [Spring Boot](https://spring.io/projects/spring-boot) based REST service that wraps the [JTok tokenizer](https://github.com/DFKI-MLT/JTok) functionality with the [ELG API](https://gitlab.com/european-language-grid/platform/elg-apis).

The service accepts a JSON [TextRequest](https://gitlab.com/european-language-grid/platform/elg-apis/blob/master/doc/T2.5-api-design.md#text-requests) on port 8080 and returns a JSON [AnnotationsResponse](https://gitlab.com/european-language-grid/platform/elg-apis/blob/master/doc/T2.5-api-design.md#annotations-response) containing the tokens, text units and paragraphs identified by JTok. The following languages are supported: `de`, `en`, `it`. For each language, a separate endpoint is provided.

The service can be tested using the provided example query `text-request.json` with the following curl command:
```
curl -H "Content-Type: application/json" -d @text-request.json http://localhost:8080/en
```

The content of `text-request.json`, containing the text content to tokenize:
```
{
  "type": "text",
  "content": "This is the text to tokenize. And here some more text.\n\nNew Paragraph"
}
```

The corresponding JSON annotations response with annotations for tokens, text units and paragraphs.
```
{
  "response": {
    "type": "annotations",
    "annotations": {
      "tokens": [{
          "start": 0,
          "end": 4,
          "features": {
            "image": "This",
            "type": "FIRST_UPPER_CASE"
          }
        }, {
          "start": 5,
          "end": 7,
          "features": {
            "image": "is",
            "type": "ALL_LOWER_CASE"
          }
        }, {
          "start": 8,
          "end": 11,
          "features": {
            "image": "the",
            "type": "ALL_LOWER_CASE"
          }
        }, {
          "start": 12,
          "end": 16,
          "features": {
            "image": "text",
            "type": "ALL_LOWER_CASE"
          }
        }, {
          "start": 17,
          "end": 19,
          "features": {
            "image": "to",
            "type": "ALL_LOWER_CASE"
          }
        }, {
          "start": 20,
          "end": 28,
          "features": {
            "image": "tokenize",
            "type": "ALL_LOWER_CASE"
          }
        }, {
          "start": 28,
          "end": 29,
          "features": {
            "image": ".",
            "type": "PERIOD"
          }
        }, {
          "start": 30,
          "end": 33,
          "features": {
            "image": "And",
            "type": "FIRST_UPPER_CASE"
          }
        }, {
          "start": 34,
          "end": 38,
          "features": {
            "image": "here",
            "type": "ALL_LOWER_CASE"
          }
        }, {
          "start": 39,
          "end": 43,
          "features": {
            "image": "some",
            "type": "ALL_LOWER_CASE"
          }
        }, {
          "start": 44,
          "end": 48,
          "features": {
            "image": "more",
            "type": "ALL_LOWER_CASE"
          }
        }, {
          "start": 49,
          "end": 53,
          "features": {
            "image": "text",
            "type": "ALL_LOWER_CASE"
          }
        }, {
          "start": 53,
          "end": 54,
          "features": {
            "image": ".",
            "type": "PERIOD"
          }
        }, {
          "start": 56,
          "end": 59,
          "features": {
            "image": "New",
            "type": "FIRST_UPPER_CASE"
          }
        }, {
          "start": 60,
          "end": 69,
          "features": {
            "image": "Paragraph",
            "type": "FIRST_UPPER_CASE"
          }
        }
      ],
      "text_units": [{
          "start": 0,
          "end": 29
        }, {
          "start": 30,
          "end": 54
        }, {
          "start": 56,
          "end": 69
        }
      ],
      "paragraphs": [{
          "start": 0,
          "end": 54
        }, {
          "start": 56,
          "end": 69
        }
      ]
    }
  }
}
```